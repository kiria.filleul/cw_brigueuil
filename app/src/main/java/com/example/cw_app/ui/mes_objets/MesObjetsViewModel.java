package com.example.cw_app.ui.mes_objets;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MesObjetsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public MesObjetsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is slideshow fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}