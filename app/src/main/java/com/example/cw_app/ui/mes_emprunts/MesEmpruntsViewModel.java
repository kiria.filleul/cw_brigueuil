package com.example.cw_app.ui.mes_emprunts;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MesEmpruntsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public MesEmpruntsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is slideshow fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}